package src.Server.Domain;


import src.Server.Vendor.LogUtil;

import java.io.*;
import java.util.zip.CRC32;
import java.util.zip.CheckedInputStream;

public class FileHandler {
    private static final int BYTESIZE = 10000;
    private LogUtil logUtil;

    public FileHandler(){
        logUtil = new LogUtil(this.getClass().getSimpleName());
    }

    public void receiveFile(InputStream is, String folder, String fileName, String size) throws IOException {
        try(
            //Streams are flushed and closed automatically after try block is done or when exception is thrown
            FileOutputStream fos = new FileOutputStream(folder + "\\" + fileName);
            BufferedOutputStream bos = new BufferedOutputStream(fos);)
        {
            logUtil.logAction("Created FileOutputStream to file: " + folder + "\\" + fileName);
            logUtil.logAction("Created BufferedOutputStream");

            byte[] contents = new byte[BYTESIZE];
            logUtil.logAction("Byte array created");

            //No of bytes read in one read() call
            int bytesRead;
            Long bytesReadTotal = Long.valueOf(0);

            logUtil.logAction("total: " + size);
            //Keep reading some number of bytes and store them in buffer, -1 means there is no more data because the end of the stream has been reached
            while(bytesReadTotal != Long.parseLong(size) && (bytesRead=is.read(contents))!=-1) {
                //Writes bytesRead bytes from the specified byte array starting at offset 0 to this buffered output stream.
                logUtil.logAction("write");
                bos.write(contents, 0, bytesRead);
                bytesReadTotal = bytesReadTotal + bytesRead;
                logUtil.logAction("BytesReadTotal: " + bytesReadTotal);
                logUtil.logAction("BytesRead: " + bytesRead);
            }
        }
    }

    /**
     * Deletes a file
     * @param fileToDelete
     * @return true if file was deleted
     */
    public Boolean deleteFile(File fileToDelete) {
        if(fileToDelete!= null && fileToDelete.exists() && fileToDelete.isFile()){
            if(fileToDelete.delete()){
                logUtil.logAction("File " + fileToDelete.getName() + " is deleted");
                return true;
            }else{
                logUtil.logAction("File " + fileToDelete.getName() + " was not deleted");
            }
        } else{
            logUtil.logAction("File " + fileToDelete.getName() + " does not exist or is no file");
        }
        return false;
    }

    /**
     * Get the checkSum of a file
     * @param file the file to read
     * @return the string representation of the checksum of the file
     */
    public static String checksumFile(File file) {
        Long checkSum = calcCRC32(file);
        if(checkSum != null) {
            return String.valueOf(checkSum);
        } else {
            return null;
        }
    }

    /**
     * Read the file and calculate the CRC32 checksum
     * @param file the file to read
     * @return the CRC32 checksum of the file or null if something went wrong
     */
    private static Long calcCRC32(File file){
        Long chksum = null;
        try {
            // Open the file and build a CRC32 checksum.
            FileInputStream fis = new FileInputStream(file);
            CRC32 chk = new CRC32();
            CheckedInputStream cis = new CheckedInputStream(fis, chk);
            byte[] buff = new byte[80];
            while (cis.read(buff) >= 0) ;
            chksum = chk.getValue();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return chksum;
    }

}
