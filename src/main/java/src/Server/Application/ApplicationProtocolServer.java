package src.Server.Application;

import src.Server.Vendor.LogUtil;

import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ApplicationProtocolServer implements IApplicationProtocolServer {
    protected static final String IPADDRESSSERVER = "127.0.0.1"; //localhost
    protected static final int PORTSERVER = 2000;

    private String serverFolder;
    private LogUtil logUtil;

    public ApplicationProtocolServer(String serverFolder){
        this.serverFolder = serverFolder;
        logUtil = new LogUtil(this.getClass().getSimpleName());
    }

    @Override
    public void setServerFolder(String folder) {
        this.serverFolder = folder;
    }

    @Override
    public String getIpAddressServer() {
        return IPADDRESSSERVER;
    }

    @Override
    public int getPortServer() {
        return PORTSERVER;
    }

    /**
     * Parse a request that the client send
     * @param serverSocket
     */
    public void parseRequest(Socket serverSocket) {
        try(
                //Streams are flushed and closed automatically after try block is done or when exception is thrown
                //Character output stream to server (for headers)
                PrintWriter out = new PrintWriter(new OutputStreamWriter(serverSocket.getOutputStream(), StandardCharsets.UTF_8), true);
                //Binary output stream to server (for requested data)
                OutputStream dataOut = new BufferedOutputStream(serverSocket.getOutputStream());
                //Read characters from the client via input stream on the socket
                BufferedReader in = new BufferedReader(new InputStreamReader(serverSocket.getInputStream(), StandardCharsets.UTF_8));
                //Read binary data from the client via input stream on the socket
                InputStream dataIn = new BufferedInputStream(serverSocket.getInputStream());)
        {
            logUtil.logAction("Created readers and writers");
            //Get request from the client
            ArrayList<String> clientRequestHeader = new ArrayList<>();
            clientRequestHeader = getClientRequestHeader(in);
            logUtil.logAction("Received request header client");
            Map<String, String>  clientRequestHeaderMap = parseClientRequestHeader(clientRequestHeader);
            logUtil.logAction("TCPClient request header parsed");
            processRequest(out, dataIn, clientRequestHeaderMap);
            logUtil.logAction("TCPClient request processed");
        } catch(IOException ioEx){
            logUtil.logAction(ioEx.getClass().getSimpleName());
        } catch (Exception ex){
            logUtil.logAction(ex.getClass().getSimpleName());
        }
    }

    /**
     * Get the header from the client request
     * @param in
     * @return
     * @throws IOException
     */
    private ArrayList<String> getClientRequestHeader(BufferedReader in) throws IOException{
        ArrayList<String> clientRequestHeader = new ArrayList<>();
        String input;
        while(!(input = in.readLine()).equals("")){
            clientRequestHeader.add(input);
            logUtil.logAction("Received line: " + input);
        }
        return clientRequestHeader;
    }

    /**
     * Parses a header sent by the client and translates the content to the returned map
     * @param requestHeader header sent by the client
     * @return map with the content of the header, if there is no content, the map is empty
     */
    public Map<String, String> parseClientRequestHeader(ArrayList<String> requestHeader) {
        Map<String, String> map = new HashMap<>();
        if(requestHeader != null && !requestHeader.isEmpty()){
            for (String requestLine : requestHeader) {
                parseClientRequestLineHeader(requestLine, map);
            }
        }
        return map;
    }

    /**
     * Parses the line of a header sent by the client and adds entries to the map
     * @param requestLine line of a header sent by the client
     * @param map will be filled with the content of the header
     */
    private void parseClientRequestLineHeader(String requestLine, Map<String, String> map){
        int indexContentType = requestLine.indexOf(":");
        int indexVerb = requestLine.indexOf(" ");

        int indexToUse = -1;
        String fieldName = "";
        String value = "";

        //Determine which separator is found
        //Always check content type first, because it's separator is not part of the first line with the verb
        if(indexContentType != -1){
            indexToUse = indexContentType;
        } else if (indexVerb != -1){
            indexToUse = indexVerb;
        }

        //Retrieve the field name and value if separators are found and content is available
        if(indexToUse != -1 && (requestLine.length() > (indexToUse + 1))){
            fieldName = requestLine.substring(0 , indexToUse).trim();
            if(requestLine.length()>indexToUse + 1){
                value = requestLine.substring(indexToUse + 1).trim();
            }
        }

        //Check if there is content for the field name and value
        if (fieldName != "" && value!= "") {
            //Always check content type first, because it's separator is not part of the first line with the verb
            if(indexContentType != -1){
                //A content field is parsed
                switch (fieldName.toUpperCase()) {
                    case "CONTENT-NAME":
                        map.put("Content-name", value);
                        logUtil.logAction("Content-name: " + value);
                        break;
                    case "CONTENT-SIZE":
                        map.put("Content-size", value);
                        logUtil.logAction("Content-size: " + value);
                        break;
                    case "CONTENT-CHECKSUM":
                        map.put("Content-checksum", value);
                        logUtil.logAction("Content-checksum: " + value);
                        break;
                    default:
                        logUtil.logAction("No match");
                }
            } else if (indexVerb != -1){
                //The first row of a header is parsed
                Pattern pattern = Pattern.compile("^SYNC/(.*)");
                Matcher matcher = pattern.matcher(fieldName);
                if(matcher.matches()){
                    map.put("Version", fieldName);
                    logUtil.logAction("Version: " + fieldName);
                    map.put("Verb",  value.toUpperCase());
                    logUtil.logAction("Verb: " + value.toUpperCase());
                } else{
                    logUtil.logAction("No match");
                }
            }
        }
    }

    /**
     * Processes the request and sends a response if necessary
     * @param out
     * @param is
     * @param requestMap
     */
    private void processRequest(PrintWriter out, InputStream is, Map<String, String> requestMap) {
        if(requestMap.get("Verb").equals("UPLOAD")){
            logUtil.logAction("Start upload processing");
            UploadRequest uploadRequest = new UploadRequest();
            ArrayList<String> responseHeader = uploadRequest.handleUploadRequest(is, serverFolder, requestMap);
            sendResponseHeader(out, responseHeader);
            logUtil.logAction("Response sent to client");
        } else if(requestMap.get("Verb").equals("DELETE")){
            logUtil.logAction("Start delete processing");
            DeleteRequest deleteRequest = new DeleteRequest();
            ArrayList<String> responseHeader = deleteRequest.handleDeleteRequest(serverFolder, requestMap);
            sendResponseHeader(out, responseHeader);
            logUtil.logAction("Response sent to client");
        }
    }

    /**
     * Send response header to client
     * @param pw
     * @param headerToClient
     */
    private void sendResponseHeader(PrintWriter pw, ArrayList<String> headerToClient){
        if(headerToClient != null){
            logUtil.logAction("Header sent:");
            for (String headerLine : headerToClient) {
                pw.println(headerLine);
                logUtil.logAction(headerLine);
            }
            pw.flush();
        }
    }

}
