package src.Server.Application;

import src.Server.Domain.FileHandler;
import src.Server.Vendor.LogUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.Map;

public class DeleteRequest {
    private FileHandler fileHandler;
    private LogUtil logUtil;

    public DeleteRequest(){
        this.fileHandler = new FileHandler();
        this.logUtil = new LogUtil(this.getClass().toString());
    }

    /**
     * Handle a delete request from the client
     * @param serverFolder
     * @param requestMap contains all the values in the header send by the client
     * @return the header to send back to the client
     */
    public ArrayList<String> handleDeleteRequest(String serverFolder, Map<String, String> requestMap){
        String fileNameToDelete = requestMap.get("Content-name");
        String pathToDelete = serverFolder + "\\" + fileNameToDelete;
        File fileToDelete = new File(pathToDelete);
        Boolean success = fileHandler.deleteFile(fileToDelete);
        return createHeaderDeleteResponse(success);
    }

    private ArrayList<String> createHeaderDeleteResponse(Boolean success) {
        ArrayList<String> deleteHeader = new ArrayList<>();
        String successString = "SYNC/1.0 ";
        if(success == true){
            successString = successString + "200";
        } else{
            successString = successString + "500";
        }
        deleteHeader.add(successString);
        deleteHeader.add("");
        return deleteHeader;
    }
}
