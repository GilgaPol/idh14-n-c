package src.Server.Application;

import src.Server.Domain.FileHandler;
import src.Server.Vendor.LogUtil;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Map;

public class UploadRequest {
    private FileHandler fileHandler;
    private LogUtil logUtil;

    public UploadRequest(){
        this.fileHandler = new FileHandler();
        this.logUtil = new LogUtil(this.getClass().toString());
    }

    /**
     * Handle a upload request from the client
     * @param is
     * @param serverFolder
     * @param requestMap contains all the values in the header send by the client
     * @return the header to send back to the client
     */
    public ArrayList<String> handleUploadRequest(InputStream is, String serverFolder, Map<String, String> requestMap){
        Boolean success = false;
        try{
            fileHandler.receiveFile(is, serverFolder, requestMap.get("Content-name"), requestMap.get("Content-size"));
            File file = new File(serverFolder + "\\" + requestMap.get("Content-name"));
            if(file.length() == Long.parseLong(requestMap.get("Content-size")) && fileHandler.checksumFile(file).equals(requestMap.get("Content-checksum"))){
                logUtil.logAction("File saved successfully");
                success = true;
            } else{
                logUtil.logAction("Something went wrong while saving the file");
            }
        } catch (IOException ioex) {
            logUtil.logAction(ioex.getStackTrace().toString());
        }
        return createHeaderUploadResponse(success);
    }

    private ArrayList<String> createHeaderUploadResponse(Boolean success) {
        ArrayList<String> uploadHeader = new ArrayList<>();
        String successString = "SYNC/1.0 ";
        if(success == true){
            successString = successString + "200";
        } else{
            successString = successString + "500";
        }
        uploadHeader.add(successString);
        uploadHeader.add("");
        return uploadHeader;
    }



}
