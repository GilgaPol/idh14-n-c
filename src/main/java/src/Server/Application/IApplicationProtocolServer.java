package src.Server.Application;

import java.net.Socket;

public interface IApplicationProtocolServer {
    void parseRequest(Socket socket);
    void setServerFolder(String folder);
    String getIpAddressServer();
    int getPortServer();
}
