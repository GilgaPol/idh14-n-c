package src.Server.Vendor;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class LogUtil {
    private String tag;
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public LogUtil(String tag){
        this.tag = tag;
    }

    public void logAction(String message){
        System.out.println(sdf.format(new Timestamp(System.currentTimeMillis())) + " " + tag + ": " + message);
    }
}
