package src.Server;


import src.Server.Infrastructure.TransportService;

import java.io.IOException;

public class MainServer {

    public static void main(String[] args) throws IOException {
        TransportService util = new TransportService();
        util.startTCPSocketServer();
    }
}
