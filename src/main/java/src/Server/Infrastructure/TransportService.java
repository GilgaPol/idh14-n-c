package src.Server.Infrastructure;

import src.Client.Infrastructure.TCPClient;

import java.io.IOException;

public class TransportService {


    /****************************************************
    ********************TCP******************************
     ****************************************************/

    public void startTCPSocketServer() throws IOException {
        TCPServer server = new TCPServer();
        server.start();
    }
}


