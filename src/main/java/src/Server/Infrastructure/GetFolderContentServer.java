package src.Server.Infrastructure;

import java.io.File;
import java.lang.String;
import java.util.Arrays;


public class GetFolderContentServer {
    public static String inputFolderName = FilePathHandlerServer.getFolderPathName();


    public static String [] getFileNames() {

        //LOAD FILE LIST
        File[] fileList = new File(inputFolderName).listFiles();

        String[] fileNameArray = new String[fileList.length];

        for (int i = 0; i < fileList.length; i++) {

            String getFileName = "" + fileList[i];

            // Get fileName
            String[] getFileNameArray = getFileName.split("/");
            String cleanFileName = "" + getFileNameArray[getFileNameArray.length-1];

            if (cleanFileName.contains(".DS_Store") == false) {
                fileNameArray[i] = cleanFileName;
            }

        }

        return fileNameArray;
    }

    public static String [] getCRC32Hash() {
        String [] cleanFileNameArray = getFileNames();

        String[] hashCRC32Array = new String[cleanFileNameArray.length];

        for (int i = 0; i < cleanFileNameArray.length; i++) {

            hashCRC32Array[i] = HashHandlerServer.doChecksum(cleanFileNameArray[i]);
        }


        return hashCRC32Array;
    }

    // For test uncomment and run
    /*
    public static void main(String[] arg){
        // Test FileName array
        System.out.println(Arrays.toString(getFileNames()));
        // Test Hash array
        System.out.println(Arrays.toString(getCRC32Hash()));
    }
    */
    
}
