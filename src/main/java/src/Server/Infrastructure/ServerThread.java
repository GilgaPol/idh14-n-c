package src.Server.Infrastructure;

import src.Server.Application.ApplicationProtocolServer;
import src.Server.Application.IApplicationProtocolServer;
import src.Server.Vendor.LogUtil;

import java.io.*;
import java.net.Socket;

public class ServerThread extends Thread {
    protected Socket socket;
    private IApplicationProtocolServer iSP;
    private LogUtil logUtil;

    public ServerThread(Socket clientSocket) {
        logUtil = new LogUtil(this.getClass().getSimpleName());
        this.socket = clientSocket;
        iSP = new ApplicationProtocolServer(FilePathHandlerServer.getFolderPathName());
    }

    public void run() {
        logUtil.logAction("Started ServerThread");
        iSP.parseRequest(socket);
        try {
            logUtil.logAction("Close socket");
            this.socket.close();
            logUtil.logAction("Socket closed");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
