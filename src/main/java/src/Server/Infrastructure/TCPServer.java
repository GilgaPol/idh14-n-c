package src.Server.Infrastructure;

import src.Server.Application.ApplicationProtocolServer;
import src.Server.Application.IApplicationProtocolServer;
import src.Server.Vendor.LogUtil;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class TCPServer {

    private IApplicationProtocolServer iSP;

    private static final String TAG = "TCPServer";
    private LogUtil logUtil;

    public TCPServer(){
        logUtil = new LogUtil(TAG);
        iSP = new ApplicationProtocolServer(FilePathHandlerServer.getFolderPathName());
    }

    public void start() {
        try(
            ServerSocket serverSocket = new ServerSocket(iSP.getPortServer());)
            {
                this.logUtil.logAction("TCPServer IP: " + InetAddress.getLocalHost().getHostAddress());
                this.logUtil.logAction("Created ServerSocket with port " + iSP.getPortServer());
            while (true) {
                Socket connectionSocket = serverSocket.accept();
                this.logUtil.logAction("Accepted connection socket");
                ServerThread thread = new ServerThread(connectionSocket);
                thread.start();
                this.logUtil.logAction("Started ServerThread socket");
                break;
            }
        } catch (IOException exception) {
            this.logUtil.logAction("ERROR: " + exception.toString());

        }
    }

}
