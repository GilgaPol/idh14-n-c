package src.Server.Infrastructure;

public class FilePathHandlerServer {

    // Create 'filetransfer_server' folder in your Documents

    public static String OSname = OSValidatorServer.getOS();

    public static String getFolderPathName() {
        if (OSname == "osx") {
            // Apple path style
            String serverFolder = System.getProperty("user.home")+"/Documents/filetransfer_server";
            return serverFolder;
        } else {
            // Windows path style
            String serverFolder = System.getProperty("user.home")+"\\Documents\\filetransfer_server";
            return serverFolder;
        }
    }

    // For test uncomment and run
    /*
    public static void main(String[] args) {
        System.out.println(getFolderPathName());

    }
    */

}

