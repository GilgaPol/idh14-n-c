package src.Server.Infrastructure;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.zip.CRC32;
import java.util.zip.CheckedInputStream;

public class HashHandlerServer {

    public static String doChecksum(String cleanFileName) {

        String filePath = FilePathHandlerServer.getFolderPathName();

        try {

            CheckedInputStream cis = null;
            try {
                cis = new CheckedInputStream(new FileInputStream(filePath+cleanFileName), new CRC32());

                Path path = Paths.get(""+filePath+cleanFileName);
                BasicFileAttributes attr = Files.readAttributes(path, BasicFileAttributes.class);

                // CRC32
                byte[] buf = new byte[1024];
                while(cis.read(buf) >= 0) {
                }

                long checksum = cis.getChecksum().getValue();
                String outputChecksum = "" + checksum;


                return outputChecksum;

            } catch (FileNotFoundException e) {
                System.err.println("File not found.");
                System.exit(1);
                return "File not found.";
            }

        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
            return "IOException";
        }

    }
}
