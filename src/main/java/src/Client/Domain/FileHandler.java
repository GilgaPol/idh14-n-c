package src.Client.Domain;


import src.Client.Vendor.LogUtil;

import java.io.*;
import java.util.concurrent.TimeUnit;
import java.util.zip.CRC32;
import java.util.zip.CheckedInputStream;

public class FileHandler {
    private static final int BYTESIZE = 10000;
    private LogUtil logUtil;

    public FileHandler(){
        logUtil = new LogUtil(this.getClass().getSimpleName());
    }


    /**
     * Send a file through the outputstream
     * @param os
     * @param file
     * @throws IOException
     */
    public void sendFile(OutputStream os, File file) throws IOException {
        try(
            //Streams are flushed and closed automatically after try block is done or when exception is thrown
            FileInputStream fis = new FileInputStream(file);
            BufferedInputStream bis = new BufferedInputStream(fis))
        {
            logUtil.logAction("Created FileInputStream");
            logUtil.logAction("Created BufferedInputStream with FileInputStream");
            byte[] contents;
            long fileLength = file.length();
            long current = 0;

            while(current!=fileLength){
                int size = BYTESIZE;
                if(fileLength - current >= size)
                    //Start at the next size block
                    current += size;
                else{
                    //Read all that is left
                    size = (int)(fileLength - current);
                    current = fileLength;
                }
                logUtil.logAction("Current: " + current);
                contents = new byte[size];
                bis.read(contents, 0, size);
                os.write(contents);
                logUtil.logAction("Sending file ... "+(current*100)/fileLength+"% complete");
            }
            //try{TimeUnit.SECONDS.sleep(10);} catch(InterruptedException ex){ ex.printStackTrace(); }
            os.flush();
            logUtil.logAction("File sent successfully");
        }
    }

    /**
     * Get the checkSum of a file
     * @param file the file to read
     * @return the string representation of the checksum of the file
     */
    public static String checksumFile(File file) {
        Long checkSum = calcCRC32(file);
        if(checkSum != null) {
            return String.valueOf(checkSum);
        } else {
            return null;
        }
    }

    /**
     * Read the file and calculate the CRC32 checksum
     * @param file the file to read
     * @return the CRC32 checksum of the file or null if something went wrong
     */
    private static Long calcCRC32(File file){
        Long chksum = null;
        try {
            // Open the file and build a CRC32 checksum.
            FileInputStream fis = new FileInputStream(file);
            CRC32 chk = new CRC32();
            CheckedInputStream cis = new CheckedInputStream(fis, chk);
            byte[] buff = new byte[80];
            while (cis.read(buff) >= 0) ;
            chksum = chk.getValue();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return chksum;
    }

}
