package src.Client.Application;

import java.net.Socket;

public interface IApplicationProtocolClient {
    Boolean sendUploadRequest(Socket clientSocket, String fileName);
    Boolean sendDeleteRequest(Socket clientSocket, String fileName);
    void setClientFolder(String folder);
    String getIpAddressServer();
    int getPortServer();
}
