package src.Client.Application;

import src.Client.Vendor.LogUtil;

import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class ApplicationProtocolClient implements IApplicationProtocolClient {
    protected static final String IPADDRESSSERVER = "127.0.0.1"; //localhost
    protected static final int PORTSERVER = 2000;

    private String clientFolder;
    private LogUtil logUtil;

    public ApplicationProtocolClient(String clientFolder){
        this.clientFolder = clientFolder;
        logUtil = new LogUtil(this.getClass().getSimpleName());
    }

    @Override
    public void setClientFolder(String folder) {
        clientFolder = folder;
    }

    @Override
    public String getIpAddressServer() {
        return IPADDRESSSERVER;
    }

    @Override
    public int getPortServer() {
        return PORTSERVER;
    }

    @Override
    /**
     * Send an upload request to the server
     * @param clientSocket
     * @param fileName
     * @return true if request was handled successfully without exceptions, even if server send negative response
     */
    public Boolean sendUploadRequest(Socket clientSocket, String fileName) {
        try (
                //Character output stream to server (for headers)
                PrintWriter out = new PrintWriter(new OutputStreamWriter(clientSocket.getOutputStream(), StandardCharsets.UTF_8), true);
                //Binary output stream to server (for requested data)
                OutputStream dataOut = new BufferedOutputStream(clientSocket.getOutputStream());
                //Read characters from the server via input stream on the socket
                BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream(), StandardCharsets.UTF_8))) {
            //Get the file that needs to be send
            File fileToUpload = new File(clientFolder + "\\" + fileName);
            if(!fileToUpload.exists()){
                throw new FileNotFoundException();
            }
            logUtil.logAction("File to send: " + clientFolder + "\\" + fileName);
            //Create the header to send to the server
            UploadRequest uploadRequest = new UploadRequest();
            ArrayList<String> uploadHeader = uploadRequest.createUploadRequestHeader(fileToUpload);
            logUtil.logAction("uploadHeader created");
            //Send the header to the server
            sendHeader(out, uploadHeader);
            logUtil.logAction("uploadHeader sent");
            //Send the body to the server
            uploadRequest.sendUploadRequestBody(dataOut, fileToUpload);
            logUtil.logAction("uploadBody sent");
            //Receive the result from the server
            ArrayList<String> responseHeader = receiveResponseHeader(in);
            logUtil.logAction("responseHeader server received");
            //Parse the response
            uploadRequest.parseUploadRequestResponseHeader(responseHeader);
            logUtil.logAction("uploadRequest completed");
            return true;
        } catch (IOException ioEx){
            logUtil.logAction(ioEx.getClass().getSimpleName());
        } catch (Exception ex){
            logUtil.logAction(ex.getClass().getSimpleName());
        }
        return false;
    }

    @Override
    /**
     * Send a delete request to the server
     * @param clientSocket
     * @param fileName
     * @return true if request was handled successfully without exceptions, even if server send negative response
     */
    public Boolean sendDeleteRequest(Socket clientSocket, String fileName) {
        try (
                //Character output stream to server (for headers)
                PrintWriter out = new PrintWriter(new OutputStreamWriter(clientSocket.getOutputStream(), StandardCharsets.UTF_8), true);
                //Binary output stream to server (for requested data)
                OutputStream dataOut = new BufferedOutputStream(clientSocket.getOutputStream());
                //Read characters from the server via input stream on the socket
                BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream(), StandardCharsets.UTF_8))) {
            //Create the header to send to the server
            DeleteRequest deleteRequest = new DeleteRequest();
            ArrayList<String> deleteHeader = deleteRequest.createDeleteRequestHeader(fileName);
            logUtil.logAction("deleteHeader created");
            //Send the header to the server
            sendHeader(out, deleteHeader);
            logUtil.logAction("deleteHeader sent");
            //Receive the result from the server
            ArrayList<String> responseHeader = receiveResponseHeader(in);
            logUtil.logAction("responseHeader server received");
            //Parse the response
            deleteRequest.parseDeleteRequestResponseHeader(responseHeader);
            logUtil.logAction("deleteRequest completed");
            return true;
        } catch (IOException ex){
            logUtil.logAction(ex.getStackTrace().toString());
        }
        return false;
    }

    /**
     * Send a header to the server through a PrintWriter
     * @param out
     * @param header
     * @throws IOException
     */
    private void sendHeader(PrintWriter out, ArrayList<String> header) {
        logUtil.logAction("Header sent:");
        for (String headerLine : header) {
            out.println(headerLine);
            logUtil.logAction(headerLine);
        }
        out.flush();
    }

    /**
     * Get the response header from the server
     * @param in
     * @throws IOException
     */
    private ArrayList<String> receiveResponseHeader(BufferedReader in) throws IOException{
        ArrayList<String> uploadResponseHeader = new ArrayList<>();
        String input;
        logUtil.logAction("Waiting for server response");
        while(!(input = in.readLine()).equals("")){
            uploadResponseHeader.add(input);
            logUtil.logAction("Received line: " + input);
        }
        return uploadResponseHeader;
    }
}
