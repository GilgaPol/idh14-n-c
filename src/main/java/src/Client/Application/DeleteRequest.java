package src.Client.Application;

import src.Client.Domain.FileHandler;
import src.Client.Vendor.LogUtil;

import java.util.ArrayList;
import java.util.StringTokenizer;

public class DeleteRequest {

    private FileHandler fileHandler;
    private LogUtil logUtil;

    public DeleteRequest() {
        this.fileHandler = new FileHandler();
        this.logUtil = new LogUtil(this.getClass().getSimpleName());
    }

    /**
     * Create the header for a delete request from the client
     * @param fileToDelete the file that should be deleted
     * @return A string ArrayList with all the header lines
     */
    public ArrayList<String> createDeleteRequestHeader(String fileToDelete){
        ArrayList<String> deleteHeader = new ArrayList<>();
        deleteHeader.add("SYNC/1.0 DELETE");
        deleteHeader.add("Content-name: "+ fileToDelete);
        deleteHeader.add("");
        return deleteHeader;
    }

    /**
     * Parse the response from the server because of the delete request
     * @param deleteResponseHeader
     * @return
     */
    public void parseDeleteRequestResponseHeader(ArrayList<String> deleteResponseHeader){
        String response = deleteResponseHeader.get(0);
        StringTokenizer parsedResponse = new StringTokenizer(response);
        if(parsedResponse.hasMoreTokens()){
            switch(parsedResponse.nextToken().toUpperCase()){
                case "SYNC/1.0":
                    logUtil.logAction("Version: SYNC/1.0");
                    parseStatus(parsedResponse.nextToken());
                    break;
                default:
                    logUtil.logAction("No match");
            }
        }
    }

    /**
     * Parse the status in the response header and perform actions if necessary
     * @param status
     * @return true if status means a positive result
     */
    public boolean parseStatus(String status){
        switch(status.toUpperCase()){
            case "200":
                logUtil.logAction("Status: " + status);
                logUtil.logAction("Request successful!");
                return true;
            case "500":
                logUtil.logAction("Status: " + status);
                logUtil.logAction("Request not successful!");
                return false;
            default:
                logUtil.logAction("No status match");
                return false;
        }
    }

}
