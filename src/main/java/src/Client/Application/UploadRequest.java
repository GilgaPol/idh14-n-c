package src.Client.Application;

import src.Client.Domain.FileHandler;
import src.Client.Vendor.LogUtil;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class UploadRequest {

    private FileHandler fileHandler;
    private LogUtil logUtil;

    public UploadRequest() {
        this.fileHandler = new FileHandler();
        this.logUtil = new LogUtil(this.getClass().toString());
    }

    /**
     * Create the header for an upload request from the client
     * @param fileToUpload the file that should be uploaded
     * @return A string ArrayList with all the header lines
     */
    public ArrayList<String> createUploadRequestHeader(File fileToUpload){
        ArrayList<String> uploadHeader = new ArrayList<>();
        uploadHeader.add("SYNC/1.0 UPLOAD");
        uploadHeader.add("Content-name: "+ fileToUpload.getName());
        uploadHeader.add("Content-size: "+ fileToUpload.length());
        uploadHeader.add("Content-checksum: "+ fileHandler.checksumFile(fileToUpload));
        uploadHeader.add("");
        return uploadHeader;
    }

    /**
     * Send an upload request body
     * @param dataOut
     * @param fileToUpload
     * @return true if file was send successfully
     */
    public boolean sendUploadRequestBody(OutputStream dataOut, File fileToUpload){
        //Send the file in bytes
        try{
            fileHandler.sendFile(dataOut, fileToUpload);
            return true;
        } catch (IOException ex){
            logUtil.logAction(ex.getClass().getSimpleName());
        }
        return false;
    }

    /**
     * Parse the response from the server because of the upload request
     * @param uploadResponseHeader
     * @return
     */
    public void parseUploadRequestResponseHeader(ArrayList<String> uploadResponseHeader){
        String response = uploadResponseHeader.get(0);
        StringTokenizer parsedResponse = new StringTokenizer(response);
        if(parsedResponse.hasMoreTokens()){
            switch(parsedResponse.nextToken().toUpperCase()){
                case "SYNC/1.0":
                    logUtil.logAction("Version: SYNC/1.0");
                    parseStatus(parsedResponse.nextToken());
                    break;
                default:
                    logUtil.logAction("No match");
            }
        }
    }

    /**
     * Parse the status in the response header and perform actions if necessary
     * @param status
     * @return true if status means a positive result
     */
    public boolean parseStatus(String status){
        switch(status.toUpperCase()){
            case "200":
                logUtil.logAction("Status: " + status);
                logUtil.logAction("Request successful!");
                return true;
            case "500":
                logUtil.logAction("Status: " + status);
                logUtil.logAction("Request not successful!");
                return false;
            default:
                logUtil.logAction("No status match");
                return false;
        }
    }
}
