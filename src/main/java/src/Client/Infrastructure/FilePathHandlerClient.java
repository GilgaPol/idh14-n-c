package src.Client.Infrastructure;

public class FilePathHandlerClient {

    // Create 'filetransfer_client' folder in your Documents


    public static String OSname = OSValidatorClient.getOS();

    public static String getFolderPathName() {
        if (OSname == "osx") {
            // Apple path style
            String clientFolder = System.getProperty("user.home")+"/Documents/filetransfer_client";
            return clientFolder;
        } else {
            // Windows path style
            String clientFolder = System.getProperty("user.home")+"\\Documents\\filetransfer_client";
            return clientFolder;
        }
    }

    // For test uncomment and run
    /*
    public static void main(String[] args) {
        System.out.println(getFolderPathName());

    }
    */


}
