package src.Client.Infrastructure;

import src.Client.Application.ApplicationProtocolClient;
import src.Client.Application.IApplicationProtocolClient;
import src.Client.Vendor.LogUtil;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;


public class TCPClient {
    private LogUtil logUtil;
    private IApplicationProtocolClient iAP;
    private final static String TAG = "TCPClient";

    public TCPClient() {
        logUtil = new LogUtil(TAG);
        //Give the protocol the folder of the client
        iAP = new ApplicationProtocolClient(FilePathHandlerClient.getFolderPathName());
    }

    public void start() {
        while (true) {
            try (
                    //Sockets are closed automatically after try block is done or when exception is thrown
                    Socket clientSocket = new Socket();)
            {
                clientSocket.connect(new InetSocketAddress(iAP.getIpAddressServer(), iAP.getPortServer()), 120000);
                logUtil.logAction("Created client socket with ip " + iAP.getIpAddressServer() + " and port " + iAP.getPortServer());
                Boolean result = iAP.sendUploadRequest(clientSocket, "test2.txt");
                if(result){
                    logUtil.logAction("sendUploadRequest completed successfully");
                } else{
                    logUtil.logAction("sendUploadRequest completed unsuccessfully");
                }
                //iAP.sendDeleteRequest(clientSocket, "test2.txt");
                //logUtil.logAction("sendDeleteRequest completed");
                break;
            } catch (IOException ioException) {
                this.logUtil.logAction("ERROR: " + ioException.getClass().getSimpleName());
                break;
            } catch (Exception exception) {
                this.logUtil.logAction("ERROR: " + exception.getClass().getSimpleName());
            }
        }
    }
}
