package src.Client;


import src.Client.Infrastructure.TransportService;

public class MainClient {

    public static void main(String[] args) {
        TransportService util = new TransportService();
        util.startTCPSocketClient();
    }
}
