package test.Server;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import src.Server.Application.DeleteRequest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class DeleteRequestTest {
    private DeleteRequest dr;

    @Rule
    public TemporaryFolder testFolder = new TemporaryFolder();

    @Before
    public void setUp() {
        dr = new DeleteRequest();
    }

    /******************************************************
     * handleDeleteRequest                                *
     *****************************************************/

    @Test
    public void handleDeleteRequest_fileExists(){
        try{
            //Arrange
            Map<String, String> requestMap = new HashMap<>();
            requestMap.put("Content-name", "test.txt");
            String testfolder = testFolder.getRoot().toPath().toString();
            testFolder.newFile("test.txt");

            //Act
            ArrayList<String> result = dr.handleDeleteRequest(testfolder, requestMap);

            //Assert
            assertEquals("SYNC/1.0 200", result.get(0));


        } catch (IOException ex){
            fail("IOException");
        }
    }

    @Test
    public void handleDeleteRequest_fileNotExists(){
        //Arrange
        Map<String, String> requestMap = new HashMap<>();
        requestMap.put("Content-name", "test.txt");
        String testfolder = testFolder.getRoot().toPath().toString();

        //Act
        ArrayList<String> result = dr.handleDeleteRequest(testfolder, requestMap);

        //Assert
        assertEquals("SYNC/1.0 500", result.get(0));
    }

}
