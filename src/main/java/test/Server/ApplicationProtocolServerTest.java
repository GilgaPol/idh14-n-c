package test.Server;


import org.junit.Before;
import org.junit.Test;
import src.Server.Application.ApplicationProtocolServer;

import java.util.ArrayList;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class ApplicationProtocolServerTest {
    private ApplicationProtocolServer aps;

    @Before
    public void setUp() {
        aps = new ApplicationProtocolServer("a");
    }

    /******************************************************
     * parseClientRequestHeader                           *
     *****************************************************/
    private ArrayList<String> createBasicHeader(){
        ArrayList<String> requestHeader = new ArrayList<>();
        requestHeader.add("SYNC/1.0 UPLOAD");
        requestHeader.add("Content-name: video.mkv");
        requestHeader.add("Content-size: 4294967296");
        requestHeader.add("Content-checksum: d969831eb8a99cff8c02e6");
        return requestHeader;
    }

    @Test
    public void parseClientRequestHeader_simpleUploadHeader(){
        //Arrange
        ArrayList<String> requestHeader = createBasicHeader();

        //Act
        Map<String, String> result = aps.parseClientRequestHeader(requestHeader);

        //Assert
        assertEquals("UPLOAD", result.get("Verb"));
        assertEquals("video.mkv", result.get("Content-name"));
        assertEquals("4294967296", result.get("Content-size"));
        assertEquals("d969831eb8a99cff8c02e6", result.get("Content-checksum"));
    }

    @Test
    public void parseClientRequestHeader_contentWithSpace(){
        //Arrange
        ArrayList<String> requestHeader = createBasicHeader();
        requestHeader.set(1,  "Content-name: vid eo.mkv");

        //Act
        Map<String, String> result = aps.parseClientRequestHeader(requestHeader);

        //Assert
        assertEquals("vid eo.mkv", result.get("Content-name"));
    }

    @Test
    public void parseClientRequestHeader_emptyValue(){
        //Arrange
        ArrayList<String> requestHeader = createBasicHeader();
        requestHeader.set(1,  "Content-name:");

        //Act
        Map<String, String> result = aps.parseClientRequestHeader(requestHeader);

        //Assert
        assertEquals(null, result.get("Content-name"));
    }

    @Test
    public void parseClientRequestHeader_noSeparator(){
        //Arrange
        ArrayList<String> requestHeader = new ArrayList<>();
        requestHeader.add("Content-name");

        //Act
        Map<String, String> result = aps.parseClientRequestHeader(requestHeader);


        //Assert
        assertEquals(true, result.isEmpty());
    }

    @Test
    public void parseClientRequestHeader_emptyLine(){
        //Arrange
        ArrayList<String> requestHeader = new ArrayList<>();
        requestHeader.add("");

        //Act
        Map<String, String> result = aps.parseClientRequestHeader(requestHeader);

        //Assert
        assertEquals(true, result.isEmpty());
    }

    @Test
    public void parseClientRequestHeader_nullHeader(){
        //Arrange
        ArrayList<String> requestHeader = null;

        //Act
        Map<String, String> result = aps.parseClientRequestHeader(requestHeader);

        //Assert
        assertEquals(true, result.isEmpty());
    }

    @Test
    public void parseClientRequestHeader_wrongProtocolName(){
        //Arrange
        ArrayList<String> requestHeader = createBasicHeader();
        requestHeader.set(0,  "TEST/1.0 UPLOAD");

        //Act
        Map<String, String> result = aps.parseClientRequestHeader(requestHeader);

        //Assert
        assertEquals(null, result.get("Version"));
        assertEquals(null, result.get("Verb"));
    }

    @Test
    public void parseClientRequestHeader_newerVersionProtocol(){
        //Arrange
        ArrayList<String> requestHeader = createBasicHeader();
        requestHeader.set(0,  "SYNC/2.0 UPLOAD");

        //Act
        Map<String, String> result = aps.parseClientRequestHeader(requestHeader);

        //Assert
        assertEquals("SYNC/2.0", result.get("Version"));
        assertEquals("UPLOAD", result.get("Verb"));
    }

    @Test
    public void parseClientRequestHeader_contentTypeNotExist(){
        //Arrange
        ArrayList<String> requestHeader = createBasicHeader();
        requestHeader.set(1,  "Content-test: test");

        //Act
        Map<String, String> result = aps.parseClientRequestHeader(requestHeader);


        //Assert
        assertEquals(null, result.get("Content-test"));
    }

    @Test
    public void parseClientRequestHeader_valueTrim(){
        //Arrange
        ArrayList<String> requestHeader = createBasicHeader();
        requestHeader.set(3,  "Content-checksum:      d969831eb8a99cff8c02e7         ");

        //Act
        Map<String, String> result = aps.parseClientRequestHeader(requestHeader);


        //Assert
        assertEquals("d969831eb8a99cff8c02e7", result.get("Content-checksum"));
    }

}
