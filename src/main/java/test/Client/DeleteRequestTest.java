package test.Client;

import org.junit.Before;
import org.junit.Test;
import src.Client.Application.DeleteRequest;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class DeleteRequestTest {
    private DeleteRequest dr;

    @Before
    public void setUp() {
        dr = new DeleteRequest();
    }

    /******************************************************
     * parseStatus                                        *
     *****************************************************/

    @Test
    public void parseStatus_status200(){
        //Arrange
        String status = "200";

        //Act
        Boolean result = dr.parseStatus(status);

        //Assert
        assertEquals(true, result);
    }

    @Test
    public void parseStatus_status500(){
        //Arrange
        String status = "500";

        //Act
        Boolean result = dr.parseStatus(status);

        //Assert
        assertEquals(false, result);
    }


    @Test
    public void parseStatus_unknownStatus(){
        //Arrange
        String status = "7000";

        //Act
        Boolean result = dr.parseStatus(status);

        //Assert
        assertEquals(false, result);
    }

    /******************************************************
     * createDeleteRequestHeader                          *
     *****************************************************/

    @Test
    public void createDeleteRequestHeader_basic(){
        //Arrange
        String filename = "test.txt";
        ArrayList<String> expectedResult = new ArrayList<>();
        expectedResult.add("SYNC/1.0 DELETE");
        expectedResult.add("Content-name: " + filename);
        expectedResult.add("");

        //Act
        ArrayList<String> result = dr.createDeleteRequestHeader(filename);

        //Assert
        assertEquals(result.size(), expectedResult.size());
        for(int i = 0; i<expectedResult.size();i++){
            assertEquals(expectedResult.get(i), result.get(i));
        }
    }

}
