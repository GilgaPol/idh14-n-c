package test.Client;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import src.Client.Application.UploadRequest;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class UploadRequestTest {
    private UploadRequest ur;

    @Before
    public void setUp() {
        ur = new UploadRequest();
    }

    /******************************************************
     * parseStatus                                        *
     *****************************************************/

    @Test
    public void parseStatus_status200(){
        //Arrange
        String status = "200";

        //Act
        Boolean result = ur.parseStatus(status);

        //Assert
        assertEquals(true, result);
    }

    @Test
    public void parseStatus_status500(){
        //Arrange
        String status = "500";

        //Act
        Boolean result = ur.parseStatus(status);

        //Assert
        assertEquals(false, result);
    }


    @Test
    public void parseStatus_unknownStatus(){
        //Arrange
        String status = "7000";

        //Act
        Boolean result = ur.parseStatus(status);

        //Assert
        assertEquals(false, result);
    }

    /******************************************************
     * createDeleteRequestHeader                          *
     *****************************************************/

    @Rule
    public TemporaryFolder folder= new TemporaryFolder();

    @Test
    public void createUploadRequestHeader(){
        try{
            //Arrange
            File createdFile = folder.newFile("test.txt");
            ArrayList<String> expectedResult = new ArrayList<>();
            expectedResult.add("SYNC/1.0 UPLOAD");
            expectedResult.add("Content-name: "+ "test.txt");
            expectedResult.add("Content-size: "+ "0");
            expectedResult.add("Content-checksum: "+ "0");
            expectedResult.add("");

            //Act
            ArrayList<String> result = ur.createUploadRequestHeader(createdFile);

            //Assert
            assertEquals(result.size(), expectedResult.size());
            for(int i = 0; i<expectedResult.size();i++){
                assertEquals(expectedResult.get(i), result.get(i));
            }
        } catch (IOException ex){
            fail("Could not create file");
        }
    }
}
